import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor() { }

  getCitySettings() {
    return environment.cities.filter( city => city.domains.includes(window.location.hostname))[0];
  }
}
