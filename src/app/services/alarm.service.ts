import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {LocationService} from './location.service';

@Injectable({
  providedIn: 'root'
})
export class AlarmService {

  city: any;

  constructor(private http: HttpClient,
              private location: LocationService) {
    this.city = location.getCitySettings();
  }

  createPerson(form: any) {
    console.log('form', form);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.http.post<any>(`${environment.url}/api/email/person/add`, JSON.stringify(form), options).toPromise();
  }


  verify(form: any) {
    console.log('form', form);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.http.post<any>(`${environment.url}/api/person/verify`, JSON.stringify(form), options).toPromise();

  }

  captcha(form: any) {
    console.log('form', form);
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.http.post<any>(`${environment.url}/api/person/question`, JSON.stringify(form), options).toPromise();
  }
}
