export const LATITUDE = 'latitude';
export const LONGITUDE = 'longitude';
export const VALID = 'VALID';
export const OK = 'ok';
export const INVALID = 'INVALID';
export const HEADER = 'header';
export let HEADER_TEXT = '';
