export const environment = {
  production: true,
  addressSearchByCoordinatesUrl: 'https://nominatim.openstreetmap.org/reverse?format=json&limit=100',
  initZoom: 14,
  url: 'http://localhost:8081'

};
